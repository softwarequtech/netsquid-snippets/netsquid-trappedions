NetSquid-TrappedIons (4.0.0)
================================

Description
-----------

This is a user contributed `snippet` for the `NetSquid quantum network simulator <https://netsquid.org>`_.

Code for simulating trapped-ion devices in quantum networks using NetSquid.

Installation
------------

See the ``INSTALL.md`` file for instruction of how to install this snippet.


Getting started
-----------------

To get started it is recommended to read the documentation.
The documentation for the master branch can be found `here <https://docs.netsquid.org/snippets/netsquid-trappedions/>`_).
The documentation contains:

* `A more in depth overview of the snippet <https://docs.netsquid.org/snippets/netsquid-trappedions/overview.html>`_
* `API documentation <https://docs.netsquid.org/snippets/netsquid-trappedions/api.html>`_.
* `Usage introduction <https://docs.netsquid.org/snippets/netsquid-trappedions/usage.html>`_.

To view the documentation of a different branch, one must locally build the documentation using python3.8:

``make docs``

The documentation will be built in the ``/docs/build/`` directory and automatically opened in the default browser, if available.
If no default browser is detected, you can manually navigate to the directory and open the ``index.html`` file in your preferred browser.

.. known-issues-start-inclusion-marker-do-not-remove

Known issues
-------------

* The ion trap collective addressing model requires that its collective instructions
  (``IonTrapMultiQubitRotation`` and ``IonTrapCollectiveMSGate``) are initialized
  and used with the number of ions in the current ion trap.
  This leads to an inability to run programs in the package if the number of ions
  is different from the supported number of ions for that program. Notably:

  * The swap program in this package only work for ion traps with two ions.
    (This program is used when an ion trap is used as a repeater node,
    so for repeater nodes only ion traps with two ions can be used)
  * The measure programs in this package only work for ion traps with one ion.

.. known-issues-end-inclusion-marker-do-not-remove

Contact
-------

For questions, please contact the maintainer: Michał van Hooft (M.K.vanHooft@tudelft.nl).

Contributors
------------

- Guus Avis
- Janice van Dam
- Michał van Hooft (M.K.vanHooft@tudelft.nl)


License
-------

The NetSquid-TrappedIons has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
