from netsquid_driver.operation_services import ProcessingNodeSwapService, ProcessingNodeMeasureService

from netsquid_trappedions.programs import IonTrapSwapProgramCollectiveAddressing, \
    IonTrapSwapProgramIndividualAddressing, IonTrapArbitraryAngleMeasureProgramCollectiveAddressing, \
    IonTrapArbitraryAngleMeasureProgramIndividualAddressing


class IonTrapMeasureServiceCollectiveAddressing(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on a trapped ion node.

    This service uses the :class:`IonTrapArbitraryAngleMeasureProgramCollectiveAddressing` that implements
    the measurement using operations available in a trapped ion with ions that are addressed collectively.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.

    """

    def __init__(self, node):
        super().__init__(node, name="IonTrapMeasureService",
                         meas_prog=IonTrapArbitraryAngleMeasureProgramCollectiveAddressing)


class IonTrapMeasureServiceIndividualAddressing(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on a trapped ion node.

    This service uses the :class:`IonTrapArbitraryAngleMeasureProgramIndividualAddressing` that implements
    the measurement using operations available in a trapped ion with individually addressable ions.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.

    """

    def __init__(self, node):
        super().__init__(node, name="IonTrapMeasureService",
                         meas_prog=IonTrapArbitraryAngleMeasureProgramIndividualAddressing)


class IonTrapSwapServiceCollectiveAddressing(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on a trapped ion node.

    This service uses the :class:`IonTrapSwapProgramCollectiveAddressing` that implements the swap
    using operations available in a trapped ion with ions that are addressed collectively.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    """

    def __init__(self, node):
        super().__init__(node, name="IonTrapSwapService", swap_prog=IonTrapSwapProgramCollectiveAddressing)


class IonTrapSwapServiceIndividualAddressing(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on a trapped ion node.

    This service uses the :class:`IonTrapSwapProgramIndividualAddressing` that implements the swap
    using operations available in a trapped ion with individually addressable ions.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    """

    def __init__(self, node):
        super().__init__(node, name="IonTrapSwapService", swap_prog=IonTrapSwapProgramIndividualAddressing)
