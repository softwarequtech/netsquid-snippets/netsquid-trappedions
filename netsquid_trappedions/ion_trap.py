from itertools import combinations
from typing import Optional

import numpy as np
from netsquid.components import IMeasureFaulty, INSTR_MEASURE
from netsquid.components.instructions import INSTR_ROT_X, INSTR_ROT_Y, INSTR_ROT_Z, INSTR_INIT, INSTR_EMIT
from netsquid.components.models import DepolarNoiseModel
from netsquid.components.qprocessor import PhysicalInstruction
from netsquid.components.qprocessor import QuantumProcessor
from netsquid_driver.measurement_services import MeasureService, SwapService
from netsquid_driver.memory_manager_implementations import MemoryManagerUsingInUseFlag
from netsquid_driver.memory_manager_service import QuantumMemoryManager
from netsquid_netbuilder.modules.qdevices import IQDeviceConfig, IQDeviceBuilder
from numpy import random

from netsquid_trappedions.instructions import IonTrapMultiQubitRotation, IonTrapCollectiveMSGate,\
    IonTrapIndividualMSGate
from netsquid_trappedions.noise_models import CollectiveDephasingNoiseModel
from netsquid_trappedions.noise_models import EmissionNoiseModel
from netsquid_trappedions.services import IonTrapSwapServiceCollectiveAddressing, \
    IonTrapSwapServiceIndividualAddressing, IonTrapMeasureServiceCollectiveAddressing, \
    IonTrapMeasureServiceIndividualAddressing


class IonTrapBaseConfig(IQDeviceConfig):
    """
    Base configuration for Ion-trap quantum processor capable of emitting entangled photons.

    This configuration contains parameters relating to number of ions, coherence, initialization, emission, measurement
    and the single qubit Z gate.

    All parameters default to their perfect values.
    That is, by default, the ion trap is initialized such that there is no noise and all gates are instantaneous.
    Note that this is unphysical.
    For realistic modelling, the ion trap should be initialized with non-perfect parameter values.

    .. note::
        The parameters relating to photon emission, `emission_fidelity`, `collection_efficiency`
        and `emission_duration`, are not directly utilized when a "magic" entanglement generation model is employed.
        These parameters and their values are made accessible for usage in the "magic" entanglement generation models,
        but their usage depends entirely on the specific entanglement generation model in use.

    """

    num_positions: int
    """Number of ions in the trap."""
    coherence_time: float = 0.
    """
    Coherence time of the qubits.
    Qubits in memory decohere according to a collective-dephasing channel characterized by this coherence time.
    This channel is both strongly correlated between all qubits in memory and non-Markovian.
    A value of zero disables decoherence effects.
    """
    prob_error_0: float = 0.
    """Measurement error probability; probability that :math:`|0>` gives outcome "1"."""
    prob_error_1: float = 0.
    """Measurement error probability; probability that :math:`|1>` gives outcome "0"."""
    init_depolar_prob: float = 0.
    """Parameter characterizing depolarizing channel that is applied to a qubit when it is initialized."""
    rot_z_depolar_prob: float = 0.
    """
    Parameter characterizing depolarizing channel that is applied to a qubit
    when a single-qubit z rotation is performed.
    """
    emission_fidelity: float = 1.
    """Fidelity of the ion-photon entangled state directly after emitting a photon."""
    collection_efficiency: float = 1.
    """Probability that an entangled photon is successfully emitted when attempted."""
    emission_duration: float = 0.
    """Time [ns] it takes to attempt emitting an entangled photon."""
    measurement_duration: float = 0.
    """Time [ns] it takes to perform a single-qubit computational-basis measurement."""
    initialization_duration: float = 0.
    """Time [ns] it takes to initialize a qubit."""
    z_rotation_duration: float = 0.
    """Time [ns] it takes to perform a single-qubit z rotation."""
    ms_pi_over_2_duration: float = 0.
    """
    Time [ns] it takes to perform a Mølmer–Sørensen gate with angle pi / 2.
    For MS gates with other angles, the duration scales proportionally to the angle,
    using this duration as the reference for pi / 2.
    """
    ms_optimization_angle: float = np.pi / 2
    """Theta angle of Mølmer–Sørensen gate for which the device has been optimized."""
    ms_depolar_prob: float = 0.
    """
    Parameter characterizing depolarizing channel that is applied to all qubits participating in a Mølmer–Sørensen gate.
    """

    external_params: Optional[dict] = None
    """
    Dictionary of extra parameters that have no direct impact on the qdevice model,
    but are physically associated with the qdevice.
    It is typically used for parameters that relate to the entanglement generation, such as `emission_fidelity`.
    """


class IonTrapCollectiveAddressingConfig(IonTrapBaseConfig):
    """
    Configuration for a collective addressing Ion-trap quantum processor.
    """
    multi_qubit_xy_rotation_depolar_prob: float = 0.
    """
    Parameter characterizing depolarizing channel that is applied to all qubits participating in a
    multi-qubit rotation around an axis in the XY plane of the Bloch sphere.
    """
    multi_qubit_xy_rotation_duration: float = 0.
    """ Time [ns] it takes to perform a multi-qubit XY rotation."""


class IonTrapIndividualAddressingConfig(IonTrapBaseConfig):
    """
    Configuration for an individual addressing Ion-trap quantum processor.
    """
    rot_x_depolar_prob: float = 0.
    """
    Parameter characterizing depolarizing channel that is applied to a qubit
    when a single-qubit x rotation is performed.
    """
    rot_y_depolar_prob: float = 0.
    """
    Parameter characterizing depolarizing channel that is applied to a qubit
    when a single-qubit y rotation is performed.
    """
    x_rotation_duration: float = 0.
    """Time [ns] it takes to perform a single-qubit x rotation."""
    y_rotation_duration: float = 0.
    """Time [ns] it takes to perform a single-qubit y rotation."""


class IonTrapBase(QuantumProcessor):
    """Base class of an ion-trap quantum processor capable of emitting entangled photons.

    This base class implements: the ion trap memory with coherence and
    the initialization, emission, measurement and the single qubit Z gate operations.
    """

    def __init__(self, config: IonTrapBaseConfig):

        # dephasing rate is used by CollectiveDephasingNoiseModel and will change when resampled
        self.add_property("dephasing_rate", value=random.normal(loc=0, scale=1), mutable=True)
        # Emission properties that may be used by qlink models, if and how depends on qlink model
        self.add_property("emission_fidelity", value=config.emission_fidelity)
        self.add_property("collection_efficiency", value=config.collection_efficiency)
        self.add_property("emission_duration", value=config.emission_duration)

        super().__init__("ion_trap_quantum_communication_device",
                         # also initialize "cavity" position used for emission
                         num_positions=config.num_positions + 1,
                         mem_noise_models=CollectiveDephasingNoiseModel(
                             coherence_time=config.coherence_time))

        self.resample()

        self.models["qout_noise_model"] = EmissionNoiseModel(
            emission_fidelity=config.emission_fidelity,
            collection_efficiency=config.collection_efficiency)

        # position of "cavity", memory position which should only be used to mediate emission.
        self.emission_position = config.num_positions
        # number of ions, i.e. "real" positions
        self.num_ions = self.num_positions - 1

        self._set_physical_instructions(config)

        if config.external_params:
            for property_name, value in config.external_params.items():
                self.add_property(property_name, value)

    def _set_physical_instructions(self, config: IonTrapBaseConfig):
        """Function that initializes the physical instructions of the ion trap."""
        # choose topologies such that auxiliary emission position ("cavity") is excluded
        one_ion_topologies = list(range(self.num_ions))
        # two_ion_topologies = list(combinations(one_ion_topologies, 2))
        any_ion_topologies = []
        for num_qubits in range(1, self.num_ions + 1):
            for topology in combinations(one_ion_topologies, num_qubits):
                any_ion_topologies.append(topology)
        emit_topologies = [(ion_position, self.emission_position) for ion_position in range(self.num_ions)]

        faulty_measurement_instruction = IMeasureFaulty("faulty_z_measurement_ion_trap",
                                                        p0=config.prob_error_0,
                                                        p1=config.prob_error_1)
        physical_measurement = PhysicalInstruction(instruction=faulty_measurement_instruction,
                                                   duration=config.measurement_duration,
                                                   quantum_noise_model=None,
                                                   classical_noise_model=None,
                                                   parallel=True,
                                                   topology=one_ion_topologies)
        self.add_composite_instruction(instruction=INSTR_MEASURE,
                                       composition=[(faulty_measurement_instruction, 0)],
                                       topology=one_ion_topologies)

        physical_init = PhysicalInstruction(instruction=INSTR_INIT,
                                            duration=config.initialization_duration,
                                            quantum_noise_model=DepolarNoiseModel(config.init_depolar_prob,
                                                                                  time_independent=True),
                                            classical_noise_model=None,
                                            parallel=True,
                                            apply_q_noise_after=True,
                                            topology=any_ion_topologies)

        z_rotation_gate = PhysicalInstruction(instruction=INSTR_ROT_Z,
                                              duration=config.z_rotation_duration,
                                              quantum_noise_model=DepolarNoiseModel(config.rot_z_depolar_prob,
                                                                                    time_independent=True),
                                              classical_noise_model=None,
                                              parallel=False,
                                              apply_q_noise_after=True,
                                              topology=one_ion_topologies)

        emit_instruction = PhysicalInstruction(instruction=INSTR_EMIT,
                                               duration=config.emission_duration,
                                               topology=emit_topologies)

        self.add_physical_instruction(physical_measurement)
        self.add_physical_instruction(physical_init)
        self.add_physical_instruction(z_rotation_gate)
        self.add_physical_instruction(emit_instruction)

    def resample(self):
        """Update this ion trap's properties by resampling the dephasing rate.

        By sampling the dephasing rate from a gaussian distribution, and by resampling after every experiment,
        we reproduce the statistics of equation (5) in the paper "Quantum repeaters based on trapped ions with
        decoherence free subspace encoding".
        """

        rate = random.normal(loc=0, scale=1)
        for memory_position_num in range(self.num_positions):
            memory_position = self.subcomponents["mem_position{}".format(memory_position_num)]
            memory_position.properties.update({"dephasing_rate": rate})
        self.properties.update({"dephasing_rate": rate})

    def reset(self):
        """Reset the ion trap and resample the dephasing rate."""
        super().reset()
        self.resample()


class IonTrapCollectiveAddressing(IonTrapBase):
    """Collective addressing ion-trap quantum processor where gates other than the Z gate, affect all ions in the trap.

    The collective addressing ion-trap adds a multi qubit XY rotation gate and
    a collective MS gate on top of the base ion trap.
    """

    def __init__(self, config: IonTrapCollectiveAddressingConfig):
        super().__init__(config)
        super()._set_physical_instructions(config)
        self._set_physical_instructions(config)

    def _set_physical_instructions(self, config: IonTrapCollectiveAddressingConfig):
        """Method that initializes the physical instructions of the collective addressing ion trap."""
        all_ion_topologies = [tuple(range(self.num_ions))]

        ms_gate_col = IonTrapCollectiveMSGate(num_positions=self.num_ions, theta=config.ms_optimization_angle)

        ms_gate_phys = PhysicalInstruction(instruction=ms_gate_col,
                                           duration=config.ms_pi_over_2_duration * (
                                                   config.ms_optimization_angle / (np.pi / 2)),
                                           quantum_noise_model=DepolarNoiseModel(config.ms_depolar_prob,
                                                                                 time_independent=True),
                                           classical_noise_model=None,
                                           parallel=False,
                                           apply_q_noise_after=True,
                                           topology=all_ion_topologies)

        multi_qubit_xy_rotation = PhysicalInstruction(instruction=IonTrapMultiQubitRotation(self.num_ions),
                                                      duration=config.multi_qubit_xy_rotation_duration,
                                                      quantum_noise_model=DepolarNoiseModel(
                                                          config.multi_qubit_xy_rotation_depolar_prob,
                                                          time_independent=True),
                                                      classical_noise_model=None,
                                                      parallel=False,
                                                      apply_q_noise_after=True,
                                                      topology=all_ion_topologies)

        self.add_physical_instruction(ms_gate_phys)
        self.add_physical_instruction(multi_qubit_xy_rotation)


class IonTrapIndividualAddressing(IonTrapBase):
    """Individual addressing ion-trap quantum processor where gates affect one or two qubits.

    The collective addressing ion-trap adds single qubit X and Y rotation gates and
    a two qubit MS gate on top of the base ion trap.
    """

    def __init__(self, config: IonTrapIndividualAddressingConfig):
        super().__init__(config)
        super()._set_physical_instructions(config)
        self._set_physical_instructions(config)

    def _set_physical_instructions(self, config: IonTrapIndividualAddressingConfig):
        """Function that initializes the physical instructions of the ion trap."""
        # choose topologies such that auxiliary emission position ("cavity") is excluded
        one_ion_topologies = list(range(self.num_ions))
        neighboring_ion_topologies = [tuple([i, i + 1]) for i in range(self.num_ions - 1)]
        neighboring_ion_topologies += [tuple([i + 1, i]) for i in range(self.num_ions - 1)]

        x_rotation_gate = PhysicalInstruction(instruction=INSTR_ROT_X,
                                              duration=config.x_rotation_duration,
                                              quantum_noise_model=DepolarNoiseModel(config.rot_x_depolar_prob,
                                                                                    time_independent=True),
                                              classical_noise_model=None,
                                              parallel=False,
                                              apply_q_noise_after=True,
                                              topology=one_ion_topologies)

        y_rotation_gate = PhysicalInstruction(instruction=INSTR_ROT_Y,
                                              duration=config.y_rotation_duration,
                                              quantum_noise_model=DepolarNoiseModel(config.rot_y_depolar_prob,
                                                                                    time_independent=True),
                                              classical_noise_model=None,
                                              parallel=False,
                                              apply_q_noise_after=True,
                                              topology=one_ion_topologies)

        ms_gate_phys = PhysicalInstruction(instruction=IonTrapIndividualMSGate(theta=config.ms_optimization_angle),
                                           duration=config.ms_pi_over_2_duration * (
                                                   config.ms_optimization_angle / (np.pi / 2)),
                                           quantum_noise_model=DepolarNoiseModel(config.ms_depolar_prob,
                                                                                 time_independent=True),
                                           classical_noise_model=None,
                                           parallel=False,
                                           apply_q_noise_after=True,
                                           topology=neighboring_ion_topologies)

        self.add_physical_instruction(x_rotation_gate)
        self.add_physical_instruction(y_rotation_gate)
        self.add_physical_instruction(ms_gate_phys)


class IonTrapCollectiveAddressingBuilder(IQDeviceBuilder):
    @classmethod
    def build(cls, name: str, qdevice_cfg: IonTrapCollectiveAddressingConfig) -> IonTrapCollectiveAddressing:
        if isinstance(qdevice_cfg, dict):
            qdevice_cfg = IonTrapCollectiveAddressingConfig(**qdevice_cfg)

        return IonTrapCollectiveAddressing(qdevice_cfg)

    @classmethod
    def build_services(cls, node):
        if node.qmemory is None:
            return
        driver = node.driver
        driver.add_service(MeasureService, IonTrapMeasureServiceCollectiveAddressing(node=node))
        driver.add_service(SwapService, IonTrapSwapServiceCollectiveAddressing(node=node))
        driver.add_service(
            QuantumMemoryManager,
            MemoryManagerUsingInUseFlag(node=node),
        )


class IonTrapIndividualAddressingBuilder(IQDeviceBuilder):
    @classmethod
    def build(cls, name: str, qdevice_cfg: IonTrapIndividualAddressingConfig) -> IonTrapIndividualAddressing:
        if isinstance(qdevice_cfg, dict):
            qdevice_cfg = IonTrapIndividualAddressingConfig(**qdevice_cfg)

        return IonTrapIndividualAddressing(qdevice_cfg)

    @classmethod
    def build_services(cls, node):
        if node.qmemory is None:
            return
        driver = node.driver
        driver.add_service(MeasureService, IonTrapMeasureServiceIndividualAddressing(node=node))
        driver.add_service(SwapService, IonTrapSwapServiceIndividualAddressing(node=node))
        driver.add_service(
            QuantumMemoryManager,
            MemoryManagerUsingInUseFlag(node=node),
        )
