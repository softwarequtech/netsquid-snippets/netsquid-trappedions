import sys
from typing import List

import matplotlib.pyplot as plt
import netsquid as ns
import numpy as np
from netsquid_netbuilder.modules.qdevices.generic import GenericQDeviceConfig, GenericQDeviceBuilder

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressingConfig, IonTrapIndividualAddressingBuilder

ns.set_qstate_formalism(ns.QFormalism.DM)

# Define the range of waiting times for simulation (in normalized time units)
# This script uses coherence_time=1 or T1,T2=1
sampling_times = np.arange(0, 1.5, 0.1)

# Define the reference Bell state and the number of qubits used
reference_state = ns.qubits.b00
reference_state_num_qubits = 2


def calculate_fidelity_ion_trap(sampling_times_: np.ndarray) -> List[float]:
    ns.sim_reset()
    fidelity_results = []

    config = IonTrapIndividualAddressingConfig(num_positions=1, coherence_time=1)
    ion_trap = IonTrapIndividualAddressingBuilder.build("Dummy", config)

    # Create qubits in the reference state
    qubits = ns.qubits.create_qubits(reference_state_num_qubits)
    ns.qubits.assign_qstate(qubits, reference_state)
    # Only put first qubit in qmemory, if there is a second qubit it does not decohere
    ion_trap.put([qubits[0]], [0])

    for sample_time in sampling_times_:
        ns.sim_run(end_time=sample_time)

        # Peek at the qubit to trigger decoherence calculation
        ion_trap.peek([0])
        fid = ns.qubits.qubitapi.fidelity(qubits, reference_state, squared=True)
        fidelity_results.append(fid)

    return fidelity_results


def calculate_fidelity_generic(sampling_times_: np.ndarray, T1: float, T2: float):
    ns.sim_reset()
    fidelity_results = []

    config = GenericQDeviceConfig(num_qubits=1, T1=T1, T2=T2)
    qdevice = GenericQDeviceBuilder.build("Dummy", config)

    # Create qubits in the reference state
    qubits = ns.qubits.create_qubits(reference_state_num_qubits)
    ns.qubits.assign_qstate(qubits, reference_state)
    # Only put first qubit in qmemory, if there is a second qubit it does not decohere
    qdevice.put([qubits[0]], [0])

    for sample_time in sampling_times_:
        ns.sim_run(end_time=sample_time)

        # Peek at the qubit to trigger decoherence calculation
        qdevice.peek([0])
        fid = ns.qubits.qubitapi.fidelity(qubits, reference_state, squared=True)
        fidelity_results.append(fid)

    return fidelity_results


# Perform averaging over multiple runs for ion trap fidelity calculations
# Each ion trap will be randomly assigned a dephasing_rate
# Thus multiple ion traps are required to get the average decoherence effect
num_trials = 1000
ion_trap_fidelities = [calculate_fidelity_ion_trap(sampling_times) for _ in range(num_trials)]
average_ion_trap_fidelities = np.average(ion_trap_fidelities, axis=0)

plt.plot(sampling_times, average_ion_trap_fidelities, label="ion trap average")

fidelities = calculate_fidelity_ion_trap(sampling_times)
plt.plot(sampling_times, fidelities, label="Ion trap example")
fidelities = calculate_fidelity_generic(sampling_times_=sampling_times, T1=1, T2=1)
plt.plot(sampling_times, fidelities, label="generic T1=t_coherence, T2=t_coherence")
fidelities = calculate_fidelity_generic(sampling_times_=sampling_times, T1=0, T2=1)
plt.plot(sampling_times, fidelities, label="generic T1=0, T2=t_coherence")

plt.ylabel("Fidelity")
plt.xlabel("Time (t/t_coherence)")
plt.ylim((0, 1))
plt.legend()

# Do not show the plot if we are testing if the examples work
if "--test_run" not in sys.argv:
    plt.show()
