import math
import sys

import matplotlib.pyplot as plt
import netsquid as ns
import numpy as np
from netsquid.components import QuantumChannel
from netsquid.components.models.qerrormodels import FibreLossModel

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressing, IonTrapIndividualAddressingConfig
from netsquid_trappedions.programs import emit_prog

"""Validate that emission success probability from an ion trap is as expected."""


def setup_ion_trap_with_fiber(collection_efficiency_, fiber_length_, attenuation_):
    config = IonTrapIndividualAddressingConfig(num_positions=1, collection_efficiency=collection_efficiency_)
    ion_trap_ = IonTrapIndividualAddressing(config)

    loss_model = FibreLossModel(p_loss_length=attenuation_, p_loss_init=0)
    fiber = QuantumChannel(name="fiber", length=fiber_length_,
                           models={"quantum_loss_model": loss_model},
                           transmit_empty_items=True)
    fiber.ports["send"].connect(ion_trap_.ports["qout"])
    collection_port_ = fiber.ports["recv"]

    return ion_trap_, collection_port_


fiber_lengths = range(1, 10)

collection_efficiency = 1
fiber_length = 1
attenuation = 0.25

success_probs = []
success_prob_errors = []
expected_success_probs = []

for fiber_length in fiber_lengths:

    ion_trap, collection_port = setup_ion_trap_with_fiber(collection_efficiency_=collection_efficiency,
                                                          fiber_length_=fiber_length,
                                                          attenuation_=attenuation)
    fail_count = 0
    num_tries = 500
    outcomes = []
    for _ in range(num_tries):
        ion_trap.execute_program(emit_prog)
        ns.sim_run()
        emitted_message = collection_port.rx_output()
        emitted_qubit = emitted_message.items[0]
        if emitted_qubit is None:
            outcomes.append(0)
        else:
            if emitted_qubit.qstate is None:
                outcomes.append(0)
            else:
                outcomes.append(1)

    success_prob = np.mean(outcomes)
    success_probs.append(success_prob)
    success_prob_error = np.std(outcomes) / math.sqrt(len(outcomes))
    success_prob_errors.append(success_prob_error)
    expected_success_prob = collection_efficiency * np.power(10, - attenuation * fiber_length / 10)
    expected_success_probs.append(expected_success_prob)

plt.figure()
plt.errorbar(fiber_lengths, y=success_probs, yerr=success_prob_errors, label="measured")
plt.plot(fiber_lengths, expected_success_probs, label="expected")
plt.xlabel("fiber length")
plt.ylabel("success probability")
plt.legend()

# Do not show the plot if we are testing if the examples work
if "--test_run" not in sys.argv:
    plt.show()
