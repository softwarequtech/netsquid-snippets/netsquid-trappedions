import sys

import matplotlib.pyplot as plt
import netsquid.qubits.qubitapi
import numpy
import numpy as np
from netsquid.components import QuantumChannel, FibreDelayModel
from netsquid.components.models.qerrormodels import FibreLossModel
from netsquid.components.qmemory import QuantumMemory
from netsquid.protocols import Protocol
from netsquid_physlayer.detectors import BSMDetector

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressing, IonTrapIndividualAddressingConfig
from netsquid_trappedions.programs import emit_prog


def initialize_network(ion_config: IonTrapIndividualAddressingConfig,
                       node_distance_: float, attenuation_: float,
                       dark_count_prob: float = 0.):
    """
    Set up a two ion trap systems with a connected BSM detector halfway.

    :param ion_config: Configuration for the ion trap system.
    :param node_distance_: Distance between the nodes (km).
    :param attenuation_: Fiber attenuation coefficient (dB/km).
    :param dark_count_prob: Probability of a false positive detection in the BSM detector due to dark counts.
    :return: Tuple containing the two ion traps and the BSM detector.
    """
    ion_trap_a_ = IonTrapIndividualAddressing(ion_config)
    ion_trap_b_ = IonTrapIndividualAddressing(ion_config)

    loss_model = FibreLossModel(p_loss_length=attenuation_, p_loss_init=0)
    delay_model = FibreDelayModel(c=200_000)  # 200,000 km/s

    fiber_a = QuantumChannel(name="fiber", length=node_distance_ / 2,
                             models={"quantum_loss_model": loss_model,
                                     "delay_model": delay_model},
                             transmit_empty_items=True)

    fiber_b = QuantumChannel(name="fiber", length=node_distance_ / 2,
                             models={"quantum_loss_model": loss_model,
                                     "delay_model": delay_model},
                             transmit_empty_items=True)

    detector_ = BSMDetector("BSM detector", p_dark=dark_count_prob)

    fiber_a.ports["send"].connect(ion_trap_a_.ports["qout"])
    fiber_b.ports["send"].connect(ion_trap_b_.ports["qout"])
    fiber_a.ports["recv"].connect(detector_.ports["qin0"])
    fiber_b.ports["recv"].connect(detector_.ports["qin1"])

    return ion_trap_a_, ion_trap_b_, detector_


class BSMResponseProtocol(Protocol):
    def __init__(self, qmem_a: QuantumMemory, qmem_b: QuantumMemory, bsm_det: BSMDetector, qubit_mem_pos: int):
        """
        Protocol that listens to a response of the BSM detector and measures the entangled qubits fidelity.

        :param qmem_a: Quantum memory of node A.
        :param qmem_b: Quantum memory of node B.
        :param bsm_det: Bell-State Measurement (BSM) detector.
        :param qubit_mem_pos: Position of the qubit in memory to be measured.
        """
        self.qmem_a = qmem_a
        self.qmem_b = qmem_b
        self.bsm_det = bsm_det
        self.qubit_mem_pos = qubit_mem_pos
        self.add_signal(netsquid.protocols.protocol.Signals.FINISHED)
        self.success = None
        self.fidelity = None

    def run(self):
        # Wait for the BSM detector to send an output message
        cout = self.bsm_det.ports["cout0"]
        yield self.await_port_output(cout)

        # Retrieve bell_index measured
        message = cout.rx_output()
        bell_index = message.items[0].bell_index

        # bell_index of -1 returned when an invalid click pattern has been measured (No entanglement)
        if bell_index == -1:
            self.success = False
            return

        self.success = True

        # Calculate fidelity of qubits in memory
        qubit_a = self.qmem_a.peek([self.qubit_mem_pos])[0]
        qubit_b = self.qmem_b.peek([self.qubit_mem_pos])[0]

        bell_state = netsquid.bell_states[bell_index]
        self.fidelity = netsquid.qubits.qubitapi.fidelity([qubit_a, qubit_b], bell_state)

    def reset(self):
        self.fidelity = None
        self.success = None


# Example parameters
num_trials = 200
memory_qubit_pos = 0
emit_qubit_pos = 1
num_ions = 1
# The emission qubit will be assigned to a position after all the ions, so: emit_qubit_pos = num_ions
attenuation = 0.2  # 0.2 db/km
dark_count_probability = 0.02
# 2% chance of measuring a photon while no photon was present, per detector in BSM detector setup.
coherence_time = 200_000  # 200 us
node_distance_list = numpy.arange(0, 100, 10)

config = IonTrapIndividualAddressingConfig(num_positions=num_ions, coherence_time=coherence_time)

avg_success_prob_list = []
avg_fidelity_list = []
err_fidelity_list = []

for node_distance in node_distance_list:
    ion_trap_a, ion_trap_b, detector = initialize_network(config, node_distance_=node_distance,
                                                          attenuation_=attenuation,
                                                          dark_count_prob=dark_count_probability)
    measure_prot = BSMResponseProtocol(ion_trap_a, ion_trap_b, detector, memory_qubit_pos)

    success_list = []
    fidelity_list = []

    for _ in range(num_trials):
        measure_prot.reset()
        ion_trap_a.execute_program(emit_prog, qubit_mapping=[memory_qubit_pos, emit_qubit_pos])
        ion_trap_b.execute_program(emit_prog, qubit_mapping=[memory_qubit_pos, emit_qubit_pos])

        measure_prot.start()
        netsquid.sim_run()
        if measure_prot.success:
            success_list.append(True)
            fidelity_list.append(measure_prot.fidelity)
        else:
            success_list.append(False)

    if len(fidelity_list) > 0:
        avg_fid = numpy.average(fidelity_list)
        err_fid = numpy.std(fidelity_list) / np.sqrt(len(fidelity_list))
    else:
        avg_fid = 0.
        err_fid = 0.

    avg_fidelity_list.append(avg_fid)
    err_fidelity_list.append(err_fid)
    avg_success_prob_list.append(numpy.average(success_list))

plt.figure()
plt.plot(node_distance_list, avg_success_prob_list)
plt.xlabel("Node distance (km)")
plt.ylabel("Success probability")
plt.ylim(0, 0.5)

# Do not show the plot if we are testing if the examples work
if "--test_run" not in sys.argv:
    plt.show()

plt.figure()
plt.errorbar(node_distance_list, y=avg_fidelity_list, yerr=err_fidelity_list, fmt='.')
plt.xlabel("Node distance (km)")
plt.ylabel("Fidelity")
plt.ylim(0, 1)

# Do not show the plot if we are testing if the examples work
if "--test_run" not in sys.argv:
    plt.show()
