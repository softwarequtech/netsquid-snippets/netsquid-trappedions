import sys

import netsquid as ns
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressingBuilder, IonTrapIndividualAddressingConfig
from programs import AliceProgram, BobProgram

ns.set_qstate_formalism(ns.QFormalism.DM)

if "--test_run" not in sys.argv:
    setup_default_console_logging(magnitude="ns", precision=1)

cfg = NetworkConfig.from_file("config.yaml")
builder = get_default_builder()
# Add the ion trap to the builder using the model name "ion_trap"
builder.register("ion_trap", IonTrapIndividualAddressingBuilder, IonTrapIndividualAddressingConfig)
network = builder.build(cfg)

alice_program = AliceProgram()
bob_program = BobProgram()

programs = {"Alice": alice_program, "Bob": bob_program}

sim_stats = run(network, programs)
