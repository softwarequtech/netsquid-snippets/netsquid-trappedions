import sys

import netsquid as ns
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.simlog_extension import setup_default_console_logging

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressingBuilder, IonTrapIndividualAddressingConfig
from programs import EPRGenerateProgram, logger

# Use density matrix formalism
ns.set_qstate_formalism(ns.QFormalism.DM)
# Show console logging of events in simulation
if "--test_run" not in sys.argv:
    setup_default_console_logging(magnitude="ms", precision=2, additional_loggers=[logger])

# Create the Netsquid network
cfg = NetworkConfig.from_file("config.yaml")
builder = get_default_builder()
# Add the ion trap to the builder using the model name "ion_trap"
builder.register("ion_trap", IonTrapIndividualAddressingBuilder, IonTrapIndividualAddressingConfig)
network = builder.build(cfg)

# Create programs to run on end nodes and run simulation
h2n0_program = EPRGenerateProgram(peer_name="h1n0", is_init_node=True, num_epr_pairs=10)
h1n0_program = EPRGenerateProgram(peer_name="h2n0", is_init_node=False, num_epr_pairs=10)

programs = {"h2n0": h2n0_program, "h1n0": h1n0_program}

sim_stats = run(network, programs)
