# Two metropolitan networks connected by a repeater chain using heralded-double-click qlinks and using ion traps for all nodes

link_config: &link_config
  # total length [km] of heralded connection (i.e. sum of fibers on both sides on midpoint station).
  length: null
  # probability that photons are lost when entering connection the connection on either side.
  p_loss_init: 0.2
  # attenuation coefficient [dB/km] of fiber on either side.
  p_loss_length: 0.20
  # speed of light [km/s] in fiber on either side.
  speed_of_light: 200_000
  # dark-count probability per detection
  dark_count_probability: 1e-9
  # probability that the presence of a photon leads to a detection event
  detector_efficiency: 0.9
  # Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability)
  visibility: 1.0
  # determines whether photon-number-resolving detectors are used for the Bell-state measurement
  num_resolving: False

ion_trap_cfg: &ion_trap_cfg
  num_positions: 4
  coherence_time: 4e9  # 4 s

  # Operation error probabilities
  prob_error_0: 0.01 # measurement error: |0> returns 1
  prob_error_1: 0.02 # measurement error: |1> returns 0
  init_depolar_prob: 0.01
  rot_z_depolar_prob: 0.005
  ms_depolar_prob: 0.03
  rot_x_depolar_prob: 0.01
  rot_y_depolar_prob: 0.01

  # Emission parameters (Will affect qlink model)
  emission_fidelity: .95
  collection_efficiency: .5
  emission_duration: 75e3  # 75 us

  # Operations execution times
  measurement_duration: 2e6  # 2 ms
  initialization_duration: 65e3  # 65 us
  z_rotation_duration: 12e3  # 12 us
  ms_pi_over_2_duration: 107e3 # 107 us
  x_rotation_duration: 12e3  # 12 us
  y_rotation_duration: 12e3  # 12 us



processing_nodes:
  - name: h1n0
    qdevice_typ: ion_trap
    qdevice_cfg:
      <<: *ion_trap_cfg
  - name: h1n1
    qdevice_typ: ion_trap
    qdevice_cfg:
      <<: *ion_trap_cfg
  - name: h2n0
    qdevice_typ: ion_trap
    qdevice_cfg:
      <<: *ion_trap_cfg
  - name: h2n1
    qdevice_typ: ion_trap
    qdevice_cfg:
      <<: *ion_trap_cfg


hubs:
  - name: h1
    connections:
      - node: h1n0
        length: 10 # km
      - node: h1n1
        length: 10 # km

    qlink_typ: heralded-double-click
    qlink_cfg:
      <<: *link_config

    clink_typ: default
    clink_cfg:
      length: null

    schedule_typ: fifo
    schedule_cfg:
      switch_time: 10e3  # 10 us
      max_multiplexing: 1

  - name: h2
    connections:
      - node: h2n0
        length: 10 # km
      - node: h2n1
        length: 10 # km

    qlink_typ: heralded-double-click
    qlink_cfg:
      <<: *link_config

    clink_typ: default
    clink_cfg:
      length: null

    schedule_typ: fifo
    schedule_cfg:
      switch_time: 10e3  # 10 us
      max_multiplexing: 1

repeater_chains:
  - metro_hub1: h1
    metro_hub2: h2

    qlink_typ: heralded-double-click
    qlink_cfg:
      <<: *link_config

    clink_typ: default
    clink_cfg:
      length: null

    repeater_nodes:
      - name: r0
        qdevice_typ: ion_trap
        qdevice_cfg:
          <<: *ion_trap_cfg

      - name: r1
        qdevice_typ: ion_trap
        qdevice_cfg:
          <<: *ion_trap_cfg

      - name: r2
        qdevice_typ: ion_trap
        qdevice_cfg:
          <<: *ion_trap_cfg

    qrep_chain_control_typ: swapASAP
    qrep_chain_control_cfg:
      cutoff_time: 0  # No cutoff
      parallel_link_generation: true

    lengths:
      - 10 # km
      - 14 # km
      - 20 # km
      - 10 # km