import logging
from typing import List, Generator

import netsquid
import numpy
from netsquid.components import INSTR_MEASURE, QuantumProgram
from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService
from netsquid_netbuilder.run import Program
from qlink_interface import ReqCreateAndKeep, ReqReceive, ResCreateAndKeep

logger = logging.getLogger(__name__)


class EPRGenerateProgram(Program):
    def __init__(self, peer_name: str, is_init_node: bool, num_epr_pairs: int = 1):
        """
        Program for generating and measuring EPR pairs and calculating QBER.

        :param peer_name: Name of the peer node.
        :param is_init_node: Whether this node is the initiator of entanglement.
        :param num_epr_pairs: Number of EPR pairs to generate.
        """
        super().__init__()
        self.peer_name = peer_name
        self.is_init_node = is_init_node
        self.num_epr_pairs = num_epr_pairs
        self.logger = None

    def run(self):
        self.logger = logger.getChild(self.context.node.name)
        measurement_results = yield from self.generate_and_measure_epr_pairs()
        qber = yield from self.calculate_qber(measurement_results)
        self.logger.info(f"Calculated QBER to be: {qber}")
        return {"qber": qber}

    def generate_and_measure_epr_pairs(self) -> Generator[None, None, List[int]]:
        """
        Generates EPR pairs and measures qubits.

        :return: Measurement results for the generated EPR pairs.
        """
        egp = self.context.egp[self.peer_name]
        qdevice = self.context.node.qdevice
        peer_id = self.context.node_id_mapping[self.peer_name]
        measurement_results = []

        # Accept all incoming requests from peer
        if not self.is_init_node:
            egp.put(ReqReceive(remote_node_id=peer_id))

        for _ in range(self.num_epr_pairs):
            if self.is_init_node:
                # create and submit a request for entanglement
                request = ReqCreateAndKeep(remote_node_id=peer_id, number=1)
                egp.put(request)
                self.logger.info(f"Submitting entanglement request to {self.peer_name}")

            # Await request completion
            yield self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__)
            response = egp.get_signal_result(label=ResCreateAndKeep.__name__, receiver=self)
            qubit_mem_pos = response.logical_qubit_id
            self.logger.info(f"Completed entanglement generation in bell state: {response.bell_state}")

            # measure the qubit
            program = QuantumProgram()
            program.apply(instruction=INSTR_MEASURE, qubit_indices=[qubit_mem_pos], output_key="m")
            qdevice.execute_program(program)
            yield self.await_program(qdevice)

            # Free the qubit and inform EntanglementTrackerService of the discard.
            self.context.node.qdevice.discard(qubit_mem_pos)
            self.context.node.driver[EntanglementTrackerService].register_local_discard_mem_pos(qubit_mem_pos)

            result = program.output['m'][0]
            # Flip the result if the Bell state is PSI_MINUS or PSI_PLUS (requires correction)
            if self.is_init_node and response.bell_state in {netsquid.BellIndex.PSI_MINUS, netsquid.BellIndex.PSI_PLUS}:
                result = int(not result)

            self.logger.info(f"Measured the qubit: {result}")
            measurement_results.append(result)

        return measurement_results

    def calculate_qber(self, results: List[int]) -> Generator[None, None, float]:
        """
        Calculates the QBER based on measurement results.

        :param results: Local measurement results.
        :return: Calculated QBER.
        """
        socket = self.context.sockets[self.peer_name]

        socket.send(results)
        peer_results = yield from socket.recv()

        if not isinstance(peer_results, list) or len(peer_results) != len(results):
            raise ValueError("Invalid results received from peer.")

        errors = [result != peer_result for result, peer_result in zip(results, peer_results)]
        qber = numpy.average(errors)

        return qber
