Usage
-----

Direct usage
+++++++++++++

This snippet can be used to directly create a quantum processor based on an ion trap.
This processor includes predefined gates, topology, and noise models that are based on an ion trap.

To create an ion trap, you must use one of the following configuration classes:
:class:`IonTrapCollectiveAddressingConfig<netsquid_trappedions.ion_trap.IonTrapCollectiveAddressingConfig>`
or
:class:`IonTrapIndividualAddressingConfig<netsquid_trappedions.ion_trap.IonTrapIndividualAddressingConfig>`.

.. code-block:: python

    config = IonTrapIndividualAddressingConfig(num_positions=1)
    ion_trap = IonTrapIndividualAddressing(config)

The ion trap can then be used like any other object of type :class:`netsquid.components.qprocessor.QuantumProcessor`,
while taking into account what instructions are implemented on the ion trap.
A detailed description of the allowed instructions can be found :ref:`here <overview_instructions>`.
This snippet contains programs specifically written with the allowed instructions to perform various operations.
These programs are documented :doc:`here <modules/programs>`.

The examples in ``examples/example_bsm_detector.py`` and ``examples/example_emission_validation.py`` show how the
ion trap can be used to emit qubits (representing photons) that are entangled with a memory qubit.

Netsquid-netbuilder
++++++++++++++++++++

To integrate the ion trap into netsquid-netbuilder, register the qdevice model as follows:

.. code-block:: python

    builder.register("ion_trap", IonTrapIndividualAddressing, IonTrapIndividualAddressingConfig)

Once registered, the model can be referenced in the network configuration YAML file by setting `qdevice_typ` to `ion_trap`.
This ensures that the specified nodes use the ion trap as their quantum device."

The example in ``/examples/two_nodes/`` shows the basic usage for a two node network and the example in
``/examples/repeater_chain/`` shows an example of a network with a repeater chain.
