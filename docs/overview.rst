Overview
---------

Introduction
++++++++++++

This package contains two classes of ion trap model: :class:`IonTrapCollectiveAddressing <netsquid_trappedions.ion_trap.IonTrapCollectiveAddressing>`
and :class:`IonTrapIndividualAddressing<netsquid_trappedions.ion_trap.IonTrapIndividualAddressing>` and the base class with the properties shared between these two models:
:class:`IonTrapBase<netsquid_trappedions.ion_trap.IonTrapBase>`.

The collective addressing ion trap only supports Z rotations,
qubit initialization and measurements as single qubit operations.
The other operations, XY rotations and Mølmer–Sørensen gate are collective and affect all qubits in the ion trap.

The individual addressing ion trap does support single qubit XY rotations and a two qubit Mølmer–Sørensen gate.

Decoherence
++++++++++++++

The ionic qubits in the ion trap undergo collective dephasing in a non-Markovian manner.
The quantum channel that describes this decoherence mechanism can be found in equation (5) of
the paper
"Quantum repeaters based on trapped ions with decoherence free subspace encoding",
Zwerger et al., https://doi.org/10.1088/2058-9565/aa7983 .
In order to model the non-Markovian process accurately, the method :meth:`IonTrapBase.resample()<netsquid_trappedions.ion_trap.IonTrapBase.resample>` should be called
with some regularity (e.g. every time when the qubits in the trap are reinitialized) or the ion traps are recreated often
(The ion trap object is sampled at initialization).
For more information about this, see the section `Decoherence modeling`_.

.. _overview_instructions:

Instructions
++++++++++++++

The instructions that can be performed on both ion traps are:

* Single-qubit Z rotations, :obj:`netsquid.components.instructions.INSTR_ROT_Z`.
* Single-qubit measurements, :obj:`netsquid.components.instructions.INSTR_MEASURE`.
* Qubit initialization, :obj:`netsquid.components.instructions.INSTR_INIT`.
* Emission of entangled photon, :class:`netsquid.components.instructions.INSTR_EMIT`.

The collective addressing ion trap also supports the following instructions:

* Multi-qubit XY rotations, :class:`netsquid_trappedions.instructions.IonTrapMultiQubitRotation`.
  That is, rotations in the Bloch sphere around any axis in the XY plane can be performed,
  but on all qubits simultaneously.
* Mølmer–Sørensen gate, :class:`netsquid_trappedions.instructions.IonTrapCollectiveMSGate`.
  This is an entangling gate that involves all qubits in the trap.
  It can be used to prepare all the ions in a GHZ state together.

The individual addressing ion trap also support the following instructions:

* Single-qubit X and Y rotations, :obj:`netsquid.components.instructions.INSTR_ROT_X` and
  :obj:`netsquid.components.instructions.INSTR_ROT_Y`.
* Mølmer–Sørensen gate, :class:`netsquid_trappedions.instructions.IonTrapIndividualMSGate`.
  This is an entangling gate that involves two neighboring qubits.

Noise on all gates is modeled as depolarizing noise.
The ion-photon state after photon emission in the presence of noise is of the Werner form
:math:`p |\Phi^+\rangle \langle\Phi^+| + (1 - p) (I / 4)`, where :math:`(I / 4)` is the maximally entangled state.

The gate set of this ion-trap quantum processor is based on the article
"A quantum information processor with trapped ions", Schindler et al.,
http://stacks.iop.org/1367-2630/15/i=12/a=123012?key=crossref.cde36010c3c4d16e566cc4e802de2091 .
The emission of photons by the ion trap is based on the article
"Tunable ion–photon entanglement in an optical cavity", Stute et al., http://www.nature.com/articles/nature11120 .

Parameters
+++++++++++

The configurations for collective and individual addressing in an ion trap are defined through the following configuration objects:
:class:`IonTrapCollectiveAddressingConfig<netsquid_trappedions.ion_trap.IonTrapCollectiveAddressingConfig>` and
:class:`IonTrapIndividualAddressingConfig<netsquid_trappedions.ion_trap.IonTrapIndividualAddressingConfig>`.
Both derive from the base class :class:`IonTrapBaseConfig<netsquid_trappedions.ion_trap.IonTrapBaseConfig>`,
which includes shared parameters applicable to both configurations.

These parameters enable customization of the number of ions in the trap, coherence times, gate execution times, and gate noise levels.
For a complete list and detailed descriptions of the available parameters, refer to the :ref:`ion trap API Documentation<API_ion_trap>`.

Extra notes
++++++++++++

Emission extra position
========================

The number of memory positions with which this QuantumProcessor is initiated is one more than specified by
the `num_positions` argument.
This extra position does not represent an ionic qubit;
instead, it is used to model the emission of an entangled photon.
The extra position should not be accessed, and none of the gates defined above can be performed on this position.
The number of "real" positions that represent ionic qubits can be obtained using :attr:`IonTrapBase.num_ions`.
The index of the extra position can be obtained using :attr:`IonTrapBase.emission_position`.

Decoherence modeling
========================

The dominant source of memory decoherence in ion traps is dephasing. One way of interpreting dephasing is
as qubits undergoing Z-rotations of unknown magnitude. In ion traps, we can approximate this rotation
to occur at a constant (but unknown) rate. There are two things that make dephasing hard to model
in ion traps, namely

1) the dephasing is collective, meaning that each ion dephases with (approximately) the same rate,
2) the probability distribution over dephasing rates is Gaussian.

The combined effect is that we have been unable to find a nice analytical solution of density matrix
evolution when there is more than one ion. Furthermore, already for one ion, the evolution becomes
non-Markovian, meaning that a noise model implementing the dephasing would need some sort of memory.

All problems are solved when we don't evolve the full density matrix, but rather sample from the
density matrix that would result from the evolution. This can be done by sampling the rotation ("dephasing")
rate ahead of time and use this rate to constantly rotate all qubits in the trap.
The downside is that we need to perform a simulation a large number of times before the statistics
reproduce the density matrix in equation (5) in the paper "Quantum repeaters based on trapped ions
with decoherence free subspace encoding" (while if we evolved the whole density matrix, the effect
of dephasing could be extracted from the density matrix obtained after a single run).

.. _overview_collective_rotation_and_ms_gate:

Collective Rotations & Mølmer–Sørensen Gate
===========================================

The collective rotations and Mølmer–Sørensen (MS) gate are defined using the **S matrix**:

.. math::

    S_\phi = \sum_{i=1}^{N} \left( \sigma^{i}_{x} \cos \phi + \sigma^{i}_{y} \sin \phi \right)

Where N is the amount of ion trap qubits, :math:`\sigma^{i}_{x}` and :math:`\sigma^{i}_{y}`
are the X and Y pauli matrices applied to the i'th qubit.
This matrix represents a collective rotation operator, where the parameter :math:`\phi` determines the rotation axis as:

.. math::

    \cos(\phi) X + \sin(\phi) Y.

Collective Rotations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The collective rotation is defined as:

.. math::

    R_{\phi}(\theta) =  e^{-i \theta S_{\phi} /2 }

The collective rotations behave like single-qubit rotation gates, :math:`R_X(\theta)` and :math:`R_Y(\theta)`,
but the same rotation is applied to all qubits.

Mølmer–Sørensen Gate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The MS gate is defined as:

.. math::

    MS_{\phi} (\theta) = e^{-i \theta S_{\phi}^{2} / 4}

Unlike the collective rotations, the MS gate entangles the qubits that participate in the gate.
Both the MS gate and collective rotations have a configurable rotation angle, :math:`\theta = t \Omega`,
which determines the magnitude of rotation and is related to the laser pulse duration.

Special Cases for the MS Gate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A particularly useful configuration is :math:`\theta = \pi / 2`, which maps the ground state :math:`|00\ldots0\rangle` to a maximally entangled **GHZ state** with an additional phase:

.. math::

    \frac{1}{\sqrt{2}} \left( |00\ldots0\rangle - i e^{iN\phi} |11\ldots1\rangle \right).

The choice of :math:`\phi` affects the gate behavior:

1. :math:`\phi = 0` → Rotation around the X-axis. For two qubits, the MS gate is equivalent to :math:`R_{XX}(\theta)`.
2. :math:`\phi = \pi /2` → Rotation around the Y-axis. For two qubits, the MS gate is equivalent to :math:`R_{YY}(\theta)`.
3. :math:`\phi = \pi / (2N)` and :math:`\theta = \pi / 2`  →  This maps :math:`|00\ldots0\rangle` to a **GHZ state without phase**:

   .. math::

       \frac{1}{\sqrt{2}} \left( |00\ldots0\rangle + |11\ldots1\rangle \right).

MS Gate in Individual Addressing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For individual addressing, the MS gate applies only to **two neighboring ion qubits**. In this case, it always behaves as either:

- :math:`R_{XX}(\theta)` when :math:`\phi = 0`
- :math:`R_{YY}(\theta)` when :math:`\phi = \pi / 2`

Further Reading
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

More details can be found in the paper
`A Quantum Information Processor with Trapped Ions <https://iopscience.iop.org/article/10.1088/1367-2630/15/12/123012>`_ (see page 6).