.. _API_ion_trap:

ion_trap
--------

.. autoclass:: netsquid_trappedions.ion_trap.IonTrapBaseConfig()
    :members:
    :show-inheritance:


.. autoclass:: netsquid_trappedions.ion_trap.IonTrapCollectiveAddressingConfig()
    :members:
    :show-inheritance:


.. autoclass:: netsquid_trappedions.ion_trap.IonTrapIndividualAddressingConfig()
    :members:
    :show-inheritance:

.. automodule:: netsquid_trappedions.ion_trap
    :members:
    :show-inheritance:
    :exclude-members: IonTrapCollectiveAddressingConfig, IonTrapIndividualAddressingConfig, IonTrapBaseConfig


