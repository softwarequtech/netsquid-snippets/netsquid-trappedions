Welcome to NetSquid-TrappedIons's documentation!
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   overview
   usage
   api
   known_issues



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
