import math
import unittest
import random
from typing import List

import netsquid as ns
import numpy as np
from netsquid_netbuilder.modules.qlinks import PerfectQLinkConfig, HeraldedDoubleClickQLinkConfig
from netsquid_netbuilder.network_config import NetworkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import calculate_fidelity_epr, fidelity_to_prob_max_mixed, \
    prob_max_mixed_to_fidelity
from netsquid_netbuilder.util.network_generation import create_2_node_network, create_metro_hub_network, \
    create_two_connected_metro_hubs_network
from netsquid_netbuilder.util.test_protocol_qlink import CreateAndKeepSenderProtocol, CreateAndKeepEventRegistration, \
    CreateAndKeepReceiveProtocol, \
    MeasureDirectlyReceiveProtocol, MeasureDirectlySenderProtocol, MeasureDirectlyEventRegistration
from qlink_interface import ResCreateAndKeep

from netsquid_trappedions.ion_trap import IonTrapIndividualAddressingConfig, IonTrapIndividualAddressingBuilder, \
    IonTrapCollectiveAddressingBuilder, IonTrapCollectiveAddressingConfig


def _multiply_fidelities_depolarizing(fids: List[float]) -> float:
    """Calculate the new fidelity from a list of fidelities assuming all noise is depolarising for a 2 qubit system."""
    probs_pure = [1-fidelity_to_prob_max_mixed(fid) for fid in fids]
    prob_pure_result = 1
    for prob in probs_pure:
        prob_pure_result *= prob

    return prob_max_mixed_to_fidelity(1-prob_pure_result)


class TestNetbuilderIntegration(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        ns.set_random_state(seed=42)
        np.random.seed(seed=42)
        random.seed(42)

        self.builder = get_default_builder()
        self.builder.register("ion_trap", IonTrapIndividualAddressingBuilder, IonTrapIndividualAddressingConfig)
        self.builder.register("ion_trap_collective", IonTrapCollectiveAddressingBuilder,
                              IonTrapCollectiveAddressingConfig)

    def tearDown(self) -> None:
        pass

    def check_epr_pairs(
            self,
            results_reg: CreateAndKeepEventRegistration,
            expected_n_epr: int,
            expected_fidelity: float,
            fidelity_delta: float
    ):

        self.assertEqual(len(results_reg.received_ck), expected_n_epr * 2)

        for i in range(expected_n_epr):
            res_egp_1 = results_reg.received_ck[i * 2]
            res_egp_2 = results_reg.received_ck[i * 2 + 1]

            self.assertEqual(res_egp_1.time, res_egp_2.time)

            res_create_keep: ResCreateAndKeep = res_egp_1.result

            fid = calculate_fidelity_epr(res_egp_1.dm, res_create_keep.bell_state)
            self.assertAlmostEqual(fid, expected_fidelity, delta=fidelity_delta)

    def check_epr_pairs_timing(
        self,
        results_reg: CreateAndKeepEventRegistration,
        n_epr: int,
        cycle_time: float,
        prob_success: float,
        tolerated_delta_avg_num_attempts: float,
    ):

        self.assertEqual(len(results_reg.received_classical), 1)
        self.assertEqual(len(results_reg.received_ck), n_epr * 2)

        res_clas = results_reg.received_classical[0]

        time = res_clas.time
        num_attempts_list = []

        for i in range(n_epr):
            res_egp_1 = results_reg.received_ck[i * 2]
            res_egp_2 = results_reg.received_ck[i * 2 + 1]

            self.assertEqual(res_egp_1.time, res_egp_2.time)

            # Calculate how long and how many attempts the entanglement generation took.
            attempt_time = res_egp_1.time - time
            num_attempts = attempt_time / cycle_time

            time = res_egp_1.time

            # Check that num_attempts is (almost) an integer. This is de-facto check that attempt_time = N * cycle_time
            self.assertTrue(math.isclose(num_attempts, round(num_attempts), abs_tol=0.001))

            num_attempts_list.append(num_attempts)

        avg_num_attempts = np.average(num_attempts_list)

        self.assertAlmostEqual(
            avg_num_attempts, 1 / prob_success, delta=tolerated_delta_avg_num_attempts
        )

    def check_measure_directly(self, event_register: MeasureDirectlyEventRegistration):
        self.assertEqual(len(event_register.submitted_md) * 2, len(event_register.received_md))

        for event in event_register.received_md:
            mirror_events = [
                temp_evt
                for temp_evt in event_register.received_md
                if temp_evt.peer_name == event.node_name
                and temp_evt.node_name == event.peer_name
                and temp_evt.result.create_id == event.result.create_id
            ]
            self.assertEqual(len(mirror_events), 1)
            mirror_event = mirror_events[0]

            if event.result.bell_state in [ns.BellIndex.PHI_PLUS, ns.BellIndex.PHI_MINUS]:
                self.assertEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )
            else:
                assert event.result.bell_state in [ns.BellIndex.PSI_PLUS, ns.BellIndex.PSI_MINUS]
                self.assertNotEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )

    def _run_create_keep_two_node_test(self, network_cfg: NetworkConfig, n_epr: int) -> CreateAndKeepEventRegistration:
        results_reg = CreateAndKeepEventRegistration()

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        return results_reg

    def test_qlink_perfect_individual(self):
        delay = 5325.73
        n_epr = 50

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2)
        qlink_cfg = PerfectQLinkConfig(state_delay=delay)
        network_cfg = create_2_node_network(qlink_typ="perfect", qlink_cfg=qlink_cfg,
                                            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_two_node_test(network_cfg, n_epr)

        self.check_epr_pairs(
            results_reg,
            n_epr,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_qlink_perfect_collective(self):
        delay = 5325.73
        n_epr = 50

        qdevice_cfg = IonTrapCollectiveAddressingConfig(num_positions=2)
        qlink_cfg = PerfectQLinkConfig(state_delay=delay)
        network_cfg = create_2_node_network(qlink_typ="perfect", qlink_cfg=qlink_cfg,
                                            qdevice_typ="ion_trap_collective", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_two_node_test(network_cfg, n_epr)

        self.check_epr_pairs(
            results_reg,
            n_epr,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_qlink_emission_fidelity(self):
        emission_fidelity = 0.85
        n_epr = 50

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2, emission_fidelity=emission_fidelity)
        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=20,
            p_loss_init=0,
            p_loss_length=0,
        )
        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg,
                                            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_two_node_test(network_cfg, n_epr)

        self.check_epr_pairs(
            results_reg,
            n_epr,
            expected_fidelity=_multiply_fidelities_depolarizing([emission_fidelity, emission_fidelity]),
            fidelity_delta=0.001,
        )

    def test_mh(self):
        node_names = ["Node0", "Node1", "Node2", "Node3"]
        node_distances = [10, 13.4, 33, 0.8]

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2)
        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            p_loss_init=0.5,
            p_loss_length=0,
        )
        network_cfg = create_metro_hub_network(
            node_names=node_names, node_distances=node_distances,
            qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg,
            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        node0_program = CreateAndKeepSenderProtocol("Node1", results_reg, n_epr)
        node1_program = CreateAndKeepReceiveProtocol("Node0", results_reg, n_epr)
        node2_program = CreateAndKeepSenderProtocol("Node3", results_reg, n_epr)
        node3_program = CreateAndKeepReceiveProtocol("Node2", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})

        self.check_epr_pairs(
            results_reg,
            expected_n_epr=n_epr * 2,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def _run_create_keep_repeater_chain_test(self, network_cfg: NetworkConfig,
                                             n_epr: int) -> CreateAndKeepEventRegistration:
        results_reg = CreateAndKeepEventRegistration()

        node0_program = CreateAndKeepSenderProtocol("Node3", results_reg, n_epr)
        node1_program = CreateAndKeepReceiveProtocol("Node2", results_reg, n_epr)
        node2_program = CreateAndKeepSenderProtocol("Node1", results_reg, n_epr)
        node3_program = CreateAndKeepReceiveProtocol("Node0", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})
        return results_reg

    def test_repeater_chain_individual(self):
        hub1_names = ["Node0", "Node1"]
        hub2_names = ["Node2", "Node3"]
        hub1_distances = [10, 13.4]
        hub2_distances = [33, 0.8]
        repeater_chain_distances = [24, 22, 10.5]
        n_epr = 10

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2)

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_names, hub1_node_distances=hub1_distances,
            hub2_node_names=hub2_names, hub2_node_distances=hub2_distances,
            repeater_chain_distances=repeater_chain_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_repeater_chain_test(network_cfg, n_epr)

        self.check_epr_pairs(
            results_reg,
            expected_n_epr=n_epr * 2,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_repeater_chain_collective(self):
        hub1_names = ["Node0", "Node1"]
        hub2_names = ["Node2", "Node3"]
        hub1_distances = [12, 11.4]
        hub2_distances = [44, 0.8]
        repeater_chain_distances = [24, 16, 10.5]
        n_epr = 10

        qdevice_cfg = IonTrapCollectiveAddressingConfig(num_positions=2)

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_names, hub1_node_distances=hub1_distances,
            hub2_node_names=hub2_names, hub2_node_distances=hub2_distances,
            repeater_chain_distances=repeater_chain_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            qdevice_typ="ion_trap_collective", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_repeater_chain_test(network_cfg, n_epr)

        self.check_epr_pairs(
            results_reg,
            expected_n_epr=n_epr * 2,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_repeater_chain_ck(self):
        hub1_names = ["Node0", "Node1"]
        hub2_names = ["Node2", "Node3"]
        hub1_distances = [10, 13.4]
        hub2_distances = [33, 0.8]
        repeater_chain_distances = [24, 22, 10.5]

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2)

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_names, hub1_node_distances=hub1_distances,
            hub2_node_names=hub2_names, hub2_node_distances=hub2_distances,
            repeater_chain_distances=repeater_chain_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = MeasureDirectlyEventRegistration()
        n_epr = 10

        node0_program = MeasureDirectlySenderProtocol("Node3", results_reg, n_epr)
        node1_program = MeasureDirectlyReceiveProtocol("Node2", results_reg, n_epr)
        node2_program = MeasureDirectlySenderProtocol("Node1", results_reg, n_epr)
        node3_program = MeasureDirectlyReceiveProtocol("Node0", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})

        self.check_measure_directly(results_reg)

    def test_emission_fidelity_nodes(self):
        length = 10
        speed_of_light = 20000
        emission_fidelity = 0.7
        n_epr = 10

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
        )
        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2, emission_fidelity=emission_fidelity)

        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg,
                                            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_two_node_test(network_cfg, n_epr)

        expected_fidelity = _multiply_fidelities_depolarizing(
            [emission_fidelity, emission_fidelity]
        )
        self.check_epr_pairs(
            results_reg,
            n_epr,
            expected_fidelity=expected_fidelity,
            fidelity_delta=0.01,
        )

    def test_qdevice_params_emission_duration_collection_efficiency(self):
        length = 2
        speed_of_light = 200000
        emission_duration = 100
        collection_efficiency = 0.5
        n_epr = 10

        t_cycle = length / speed_of_light * 1e9 + emission_duration

        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            length=length,
            p_loss_init=0,
            p_loss_length=0,
            speed_of_light=speed_of_light,
        )

        qdevice_cfg = IonTrapIndividualAddressingConfig(num_positions=2, emission_duration=emission_duration,
                                                        collection_efficiency=collection_efficiency)

        network_cfg = create_2_node_network(qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg,
                                            qdevice_typ="ion_trap", qdevice_cfg=qdevice_cfg)

        results_reg = self._run_create_keep_two_node_test(network_cfg, n_epr)

        prob_success = 1/2 * collection_efficiency * collection_efficiency

        self.check_epr_pairs_timing(
            results_reg,
            n_epr,
            cycle_time=t_cycle,
            prob_success=prob_success,
            tolerated_delta_avg_num_attempts=2 / np.sqrt(n_epr) * 1 / prob_success,
        )
