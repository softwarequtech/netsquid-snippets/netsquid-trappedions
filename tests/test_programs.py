import logging
import unittest

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import numpy as np
from netsquid import BellIndex
from netsquid.components import INSTR_INIT, INSTR_ROT_X, INSTR_ROT_Z, ProcessorStatus
from netsquid.qubits import ketstates

from netsquid_trappedions.instructions import INSTR_INIT_BELL, IonTrapMultiQubitRotation
from netsquid_trappedions.ion_trap import IonTrapBase, IonTrapBaseConfig, IonTrapCollectiveAddressing, \
    IonTrapCollectiveAddressingConfig, \
    IonTrapIndividualAddressing, IonTrapIndividualAddressingConfig
from netsquid_trappedions.programs import ion_trap_swap_program_collective_addressing, \
    ion_trap_swap_program_individual_addressing, IonTrapZMeasureProgram, \
    IonTrapXMeasureProgramCollectiveAddressing, IonTrapXMeasureProgramIndividualAddressing, \
    ion_trap_one_qubit_hadamard_collective_addressing, \
    ion_trap_one_qubit_hadamard_individual_addressing, emit_prog, \
    IonTrapArbitraryAngleMeasureProgramCollectiveAddressing, \
    IonTrapArbitraryAngleMeasureProgramIndividualAddressing


class TestIonTrapSwapProgramCollectiveAddressing(unittest.TestCase):
    def _initialize_ion_trap(self, num_ions: int):
        self.ion_trap = IonTrapCollectiveAddressing(IonTrapCollectiveAddressingConfig(num_positions=num_ions))
        for pos in range(num_ions):
            self.ion_trap.execute_instruction(INSTR_INIT, qubit_mapping=[pos])
            ns.sim_run()
        self.ion_trap.add_instruction(INSTR_INIT_BELL, duration=0)

    def setUp(self) -> None:
        ns.sim_reset()
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
        self.SwapProgram = ion_trap_swap_program_collective_addressing

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_swap(self):
        self._initialize_ion_trap(num_ions=2)
        for i in range(4):
            bell_index = BellIndex(i)
            self.ion_trap.execute_instruction(INSTR_INIT_BELL, bell_index=bell_index)
            ns.sim_run()
            self.ion_trap.execute_program(self.SwapProgram)
            self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
            ns.sim_run()
            self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
            self.assertEqual(self.SwapProgram.outcome_as_netsquid_bell_index, bell_index)

    @unittest.expectedFailure
    def test_swap_2(self):
        self._initialize_ion_trap(num_ions=4)
        for i in range(4):
            bell_index = BellIndex(i)
            self.ion_trap.execute_instruction(INSTR_INIT_BELL, bell_index=bell_index)
            ns.sim_run()
            self.ion_trap.execute_program(self.SwapProgram)
            self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
            ns.sim_run()
            self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
            self.assertEqual(self.SwapProgram.outcome_as_netsquid_bell_index, bell_index)


class TestIonTrapSwapProgramIndividualAddressing(unittest.TestCase):
    def _initialize_ion_trap(self, num_ions: int):
        self.ion_trap = IonTrapIndividualAddressing(IonTrapIndividualAddressingConfig(num_positions=num_ions))
        for pos in range(num_ions):
            self.ion_trap.execute_instruction(INSTR_INIT, qubit_mapping=[pos])
            ns.sim_run()
        self.ion_trap.add_instruction(INSTR_INIT_BELL, duration=0)

    def setUp(self) -> None:
        ns.sim_reset()
        ns.qubits.qformalism.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
        self.SwapProgram = ion_trap_swap_program_individual_addressing

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_swap(self):
        self._initialize_ion_trap(num_ions=2)
        for i in range(4):
            bell_index = BellIndex(i)
            self.ion_trap.execute_instruction(INSTR_INIT_BELL, bell_index=bell_index)
            ns.sim_run()
            self.ion_trap.execute_program(self.SwapProgram, qubit_mapping=[0, 1])
            self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
            ns.sim_run()
            self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
            self.assertEqual(self.SwapProgram.outcome_as_netsquid_bell_index, bell_index)

    def test_swap_2(self):
        self._initialize_ion_trap(num_ions=4)
        for i in range(4):
            bell_index = BellIndex(i)
            self.ion_trap.execute_instruction(INSTR_INIT_BELL, bell_index=bell_index)
            ns.sim_run()
            self.ion_trap.execute_program(self.SwapProgram, qubit_mapping=[0, 1])
            self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
            ns.sim_run()
            self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
            self.assertEqual(self.SwapProgram.outcome_as_netsquid_bell_index, bell_index)

    def test_swap_3(self):
        self._initialize_ion_trap(num_ions=2)
        for i in range(4):
            bell_index = BellIndex(i)
            self.ion_trap.execute_instruction(INSTR_INIT_BELL, bell_index=bell_index)
            ns.sim_run()
            self.ion_trap.execute_program(self.SwapProgram, qubit_mapping=[1, 0])
            self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
            ns.sim_run()
            self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
            self.assertEqual(self.SwapProgram.outcome_as_netsquid_bell_index, bell_index)


class TestIonTrapMeasurementProgramCollectiveAddressing(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()

    def _initialize_ion_trap(self, num_ions: int):
        self.ion_trap = IonTrapCollectiveAddressing(IonTrapCollectiveAddressingConfig(num_positions=num_ions))
        for pos in range(num_ions):
            self.ion_trap.execute_instruction(INSTR_INIT, qubit_mapping=[pos])
            ns.sim_run()

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_z_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    def test_z_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_instruction(IonTrapMultiQubitRotation(num_positions=1), phi=0, theta=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_z_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_instruction(IonTrapMultiQubitRotation(num_positions=2), phi=0, theta=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_x_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapXMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_collective_addressing)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    def test_x_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapXMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_instruction(IonTrapMultiQubitRotation(num_positions=1), phi=0, theta=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_collective_addressing)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    @unittest.expectedFailure
    def test_x_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapXMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_instruction(IonTrapMultiQubitRotation(num_positions=2), phi=0, theta=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_collective_addressing)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_arbitrary_angle_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapArbitraryAngleMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi / 2,
                                      y_rotation_angle=0, x_rotation_angle_2=np.pi / 2)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)

        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_arbitrary_angle_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapArbitraryAngleMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi,
                                      y_rotation_angle=np.pi, x_rotation_angle_2=0)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    @unittest.expectedFailure
    def test_arbitrary_angle_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapArbitraryAngleMeasureProgramCollectiveAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi,
                                      y_rotation_angle=np.pi, x_rotation_angle_2=0)

        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)

        result = program.output["outcome"][0]
        self.assertEqual(result, 0)


class TestIonTrapMeasurementProgramIndividualAddressing(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()

    def _initialize_ion_trap(self, num_ions: int):
        self.ion_trap = IonTrapIndividualAddressing(IonTrapIndividualAddressingConfig(num_positions=num_ions))
        for pos in range(num_ions):
            self.ion_trap.execute_instruction(INSTR_INIT, qubit_mapping=[pos])
            ns.sim_run()

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_z_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    def test_z_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_instruction(INSTR_ROT_X, angle=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_z_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapZMeasureProgram()
        self.ion_trap.execute_instruction(INSTR_ROT_X, angle=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_x_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapXMeasureProgramIndividualAddressing()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_individual_addressing)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    def test_x_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapXMeasureProgramIndividualAddressing()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_individual_addressing)
        ns.sim_run()
        self.ion_trap.execute_instruction(INSTR_ROT_Z, angle=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_x_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapXMeasureProgramIndividualAddressing()
        self.ion_trap.execute_instruction(INSTR_ROT_X, qubit_mapping=[0], angle=np.pi)
        ns.sim_run()
        self.ion_trap.execute_instruction(INSTR_ROT_X, qubit_mapping=[1], angle=np.pi)
        ns.sim_run()
        self.ion_trap.execute_program(ion_trap_one_qubit_hadamard_individual_addressing)
        ns.sim_run()
        self.ion_trap.execute_program(program)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_arbitrary_angle_meas_0(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapArbitraryAngleMeasureProgramIndividualAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi / 2,
                                      y_rotation_angle=0, x_rotation_angle_2=np.pi / 2)
        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)

        result = program.output["outcome"][0]
        self.assertEqual(result, 1)

    def test_arbitrary_angle_meas_1(self):
        self._initialize_ion_trap(num_ions=1)
        program = IonTrapArbitraryAngleMeasureProgramIndividualAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi,
                                      y_rotation_angle=np.pi, x_rotation_angle_2=0)

        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)

    def test_arbitrary_angle_meas_2(self):
        self._initialize_ion_trap(num_ions=2)
        program = IonTrapArbitraryAngleMeasureProgramIndividualAddressing()
        self.ion_trap.execute_program(program, qubit_mapping=[0], x_rotation_angle_1=np.pi,
                                      y_rotation_angle=np.pi, x_rotation_angle_2=0)

        self.assertEqual(self.ion_trap.status, ProcessorStatus.RUNNING)
        ns.sim_run()
        self.assertEqual(self.ion_trap.status, ProcessorStatus.IDLE)
        result = program.output["outcome"][0]
        self.assertEqual(result, 0)


# This test should be restored
class TestIonTrapEmissionProgram(unittest.TestCase):
    """
    This test check that when ions emit photons, this happens with a success chance between zero and one,
    and that the photon and ion have fidelity>0.5 to the phi+ Bell state.
    """

    def setUp(self) -> None:
        ns.sim_reset()
        config = IonTrapBaseConfig(num_positions=1)
        self.ion_trap = IonTrapBase(config)

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_ion_trap_emission_program(self):
        self.ion_trap.execute_program(emit_prog, qubit_mapping=[0, self.ion_trap.emission_position])
        ns.sim_run()
        message = self.ion_trap.ports["qout"].rx_output()
        self.assertIsNotNone(message)
        qubit_ion = self.ion_trap.peek([0])[0]
        qubit_photon = message.items[0]
        fidelity = qapi.fidelity([qubit_ion, qubit_photon], ketstates.b00)
        self.assertAlmostEqual(fidelity, 1)


if __name__ == "__main__":
    ns.logger.setLevel(logging.DEBUG)
    unittest.main()
